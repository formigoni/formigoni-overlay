# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="Mailspring is a new version of Nylas Mail maintained by one of the original authors."
HOMEPAGE="https://getmailspring.com/"
SRC_URI="https://github.com/Foundry376/Mailspring/releases/download/1.2.1/mailspring-1.2.1-0.1.x86_64.rpm"

LICENSE="GPL"
SLOT="0"
KEYWORDS="amd64"

S=${WORKDIR}/usr/

inherit rpm

src_unpack () {
	rpm_src_unpack ${S}
	cd "${S}"
}

src_install(){
	cp -R "${S}/" "${D}/"
}
