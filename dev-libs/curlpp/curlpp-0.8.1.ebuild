# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="C++ wrapper around libcURL"
HOMEPAGE="http://www.curlpp.org"
SRC_URI="https://github.com/jpbarrette/curlpp/archive/v0.8.1.tar.gz -> curlpp-0.8.1.tar.gz"

SLOT="0"
KEYWORDS="~amd64"

inherit cmake-utils

src_configure(){
	cmake-utils_src_configure
}

src_compile(){
	cmake-utils_src_make
}

src_install(){
	cmake-utils_src_install
}
