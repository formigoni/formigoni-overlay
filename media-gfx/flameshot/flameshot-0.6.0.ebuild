# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="Powerful yet simple to use screenshot software."
HOMEPAGE="https://github.com/lupoDharkael/flameshot"
SRC_URI="https://github.com/lupoDharkael/flameshot/archive/v0.6.0.tar.gz"

LICENSE="GPLv3"
SLOT="0"
KEYWORDS="~amd64"

src_compile(){
	mkdir build && cd build
	qmake ../
	make
}

src_install(){
	dobin "${S}/build/flameshot"
}
