# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="Tool for lossless trimming/cutting of video and audio files"
HOMEPAGE="https://github.com/mifi/lossless-cut"
SRC_URI="https://github.com/mifi/lossless-cut/releases/download/v1.14.0/LosslessCut-linux-x64.zip -> lossless-cut.zip
		https://gitlab.com/formigoni/formigoni-overlay/raw/master/media-gfx/lossless-cut/lossless-cut.svg
		https://gitlab.com/formigoni/formigoni-overlay/raw/master/media-gfx/lossless-cut/lossless-cut.desktop"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

inherit xdg-utils

S="${WORKDIR}/LosslessCut-linux-x64"

src_install(){
	dodir "/usr/lib/lossless-cut"
	dodir "/usr/share/applications"
	dodir "/usr/share/pixmaps"

	cp  "${DISTDIR}/lossless-cut.desktop" "${D}/usr/share/applications"
	cp  "${DISTDIR}/lossless-cut.svg" "${D}/usr/share/pixmaps"
	cp -r "${S}/." "${D}/usr/lib/lossless-cut"

	dosym "/usr/lib/lossless-cut/LosslessCut" "/usr/bin/losslesscut"
}

pkg_postinst() {
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_desktop_database_update
}
