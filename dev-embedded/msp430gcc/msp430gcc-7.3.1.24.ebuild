# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="GCC - Open Source Compiler for MSP Microcontrollers"
HOMEPAGE="http://www.ti.com/tool/MSP430-GCC-OPENSOURCE"
SRC_URI="http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSPGCC/latest/exports/msp430-gcc-7.3.1.24_linux64.tar.bz2 -> msp430gcc-7.3.1.24.tar.bz2
		http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSPGCC/latest/exports/msp430-gcc-support-files-1.205.zip -> msp430gcc-headers.zip"

LICENSE="GPLv2"
SLOT="0"
KEYWORDS="amd64"

S="${WORKDIR}"

src_configure(){
	cp -r "${WORKDIR}/msp430-gcc-support-files/include/" "${WORKDIR}/msp430-gcc-7.3.1.24_linux64/"
}
src_install(){
	dodir "/usr/bin"
	dodir "/usr/msp430gcc"
	cp -r "${WORKDIR}/msp430-gcc-7.3.1.24_linux64/." "${D}/usr/msp430gcc"
	dosym "/usr/msp430gcc/bin/msp430-elf-gcc" "/usr/bin/msp430-elf-gcc"
}
