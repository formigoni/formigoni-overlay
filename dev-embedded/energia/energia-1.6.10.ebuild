# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="Energia is an open-source electronics prototyping platform started by Robert Wessels in January of 2012"
HOMEPAGE="http://energia.nu"
SRC_URI="http://energia.nu/downloads/downloadv4.php?file=energia-1.6.10E18-linux64.tar.xz -> energia-1.6.10E18-linux64.tar.xz
		https://gitlab.com/formigoni/formigoni-overlay/raw/d8f25867f0c9e762ccacfaa4cdc7e7d39309e9b3/dev-embedded/energia/energia.desktop -> energia.desktop"

LICENSE="GPLv2"
SLOT="0"
KEYWORDS="~amd64"

S="${WORKDIR}/energia-1.6.10E18"

inherit xdg-utils

src_install(){
	# Create Dirs
	mkdir -p "${D}/usr/lib/energia"
	mkdir -p "${D}/usr/share/pixmaps"
	mkdir "${D}/usr/share/applications"
	mkdir "${D}/usr/bin"

	# Copy Files
	cp -R "${S}/." "${D}/usr/lib/energia"
	cp "${DISTDIR}/energia.desktop" "${D}/usr/share/applications"

	# Create Symlinks
	dosym "/usr/lib/energia/energia" "/usr/bin/energia"
	dosym "/usr/lib/energia/lib/icons/64x64/apps/energia.png" "/usr/share/pixmaps/energia.png"
}

pkg_postinst() {
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_desktop_database_update
}
