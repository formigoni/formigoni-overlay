# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="The note-taking app for programmers"
HOMEPAGE="https://boostnote.io/"
SRC_URI="https://github.com/BoostIO/boost-releases/releases/download/v0.11.9/boostnote_0.11.9_amd64.deb -> boostnote-0.11.9.deb"

LICENSE="GPLv3"
SLOT="0"
KEYWORDS="~amd64"

S="${WORKDIR}/usr/"

inherit xdg-utils

src_unpack(){
	unpack "boostnote-0.11.9.deb"
	unpack "${WORKDIR}/data.tar.xz"
}

src_install(){
	# .desktop Icon
	insinto "/usr/share/pixmaps/"
	doins "${S}/share/pixmaps/boostnote.png"
	# .desktop File
	insinto "/usr/share/applications/"
	doins "${S}/share/applications/boostnote.desktop"
	# Misc Libraries & Files
	insinto "/usr/share/"
	doins -r "${S}/share/boostnote"
	fperms +x "/usr/share/boostnote/Boostnote"
	# Binary File Symlink
	dosym "/usr/share/boostnote/Boostnote" "/usr/bin/boostnote"
}

pkg_postinst() {
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_desktop_database_update
}
