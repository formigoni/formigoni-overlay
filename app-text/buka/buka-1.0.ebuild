# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="Buka is a modern software that helps you manage your ebook at ease."
HOMEPAGE="https://github.com/oguzhaninan/Buka"
SRC_URI="https://github.com/oguzhaninan/Buka/releases/download/v1.0.0/Buka_1.0.0_amd64.deb"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64"

S="${WORKDIR}/"

src_install(){
	tar -xf data.tar.xz
	cp -R "${WORKDIR}/usr/" "${D}/"
}
