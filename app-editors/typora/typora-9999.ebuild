# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="Typora is a minimal markdown editor"
HOMEPAGE="https://typora.io/"
SRC_URI="https://typora.io/linux/Typora-linux-x64.tar.gz
         https://gitlab.com/formigoni/formigoni-overlay/raw/master/app-editors/typora/typora.desktop -> typora.desktop"

LICENSE="Comercial"
SLOT="0"
KEYWORDS="amd64"

S="${WORKDIR}/Typora-linux-x64/"

inherit xdg-utils

src_install(){
	dodir "/usr/share/applications"
	dodir "/usr/share/pixmaps"
	dodir "/usr/lib/typora"

	cp -r "${S}/." "${D}/usr/lib/typora"
	cp "${S}/resources/app/asserts/icon/icon_128x128.png" "${D}/usr/share/pixmaps/typora.png"
	cp "${DISTDIR}/typora.desktop" "${D}/usr/share/applications"

	dosym "/usr/lib/typora/Typora" "/usr/bin/typora"
}

pkg_postinst() {
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_desktop_database_update
}
