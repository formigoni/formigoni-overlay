# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="A diagram creation tool"
HOMEPAGE="http://astah.net/"
SRC_URI="http://cdn.astah.net/downloads/astah-uml-7.2.0.1ff236-0.noarch.rpm"

LICENSE="Trial"
SLOT="0"
KEYWORDS="~amd64"

S="${WORKDIR}/usr/"

inherit rpm xdg-utils

src_unpack () {
	rpm_src_unpack ${S}
}

src_install() {
	cp -R "${S}/" "${D}/"
}

pkg_postinst() {
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_mimeinfo_database_update
	xdg_desktop_database_update
}
