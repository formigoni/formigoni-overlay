# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="A diagram creation tool"
HOMEPAGE="http://astah.net/"
SRC_URI="http://cdn.change-vision.com/files/astah-professional-7.2.0.1ff236-0.noarch.rpm"

LICENSE="Trial"
SLOT="0"
KEYWORDS="amd64"
S=${WORKDIR}/usr/

inherit rpm

src_unpack () {
	rpm_src_unpack ${S}
	cd "${S}"
}

src_install(){
	cp -R "${S}/" "${D}/"
}
