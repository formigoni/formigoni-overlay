# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
EAPI=6

DESCRIPTION="The legendary Git GUI client for Windows, Mac and Linux"
HOMEPAGE="https://www.gitkraken.com/"
SRC_URI="https://release.gitkraken.com/linux/gitkraken-amd64.deb"

LICENSE="gitkraken-EULA"
SLOT="0"

S="${WORKDIR}/usr/"

src_unpack () {
	unpack "${DISTDIR}/gitkraken-amd64.deb"
	unpack "${WORKDIR}/data.tar.xz"
}

src_install(){
    cp -R "${S}/" "${D}/"
}
